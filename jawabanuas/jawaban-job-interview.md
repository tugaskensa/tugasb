# No.1
![](https://gitlab.com/tugaskensa/tugasb/-/raw/main/dokumentasi/Screenshot__58_.png)
![](https://gitlab.com/tugaskensa/tugasb/-/raw/main/dokumentasi/usecase3.jpg)
![](https://gitlab.com/tugaskensa/tugasb/-/raw/main/dokumentasi/usecase4.jpg)

# No.2
![](https://gitlab.com/tugaskensa/tugasb/-/raw/main/dokumentasi/classdiagramcookpad.drawio.png)

# No.3
Single Responsibility Principle (SRP): sebuah kelas seharus memiliki satu dan hanya bertanggung jawab atas satu hal. Pada program ini saya membuat kelas yang satu dan hanya satu metod yang berfungsi untuk berpindah scene.
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/SRP.gif)

Open-Closed Principle (OCP): Program terbuka untuk perluasan tetapi tertutup untuk modifikasi. pada program ini saya memisahkan setiap metod yang berfungsi berinteraksi dengan data base, contohnya ialah metod simpan, update, dll. agar kelak dapat memodifikasi kode dengan mudah.
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/OCP.gif)

Liskov Substitution Principle (LSP): Subkelas harus dapat digunakan sebagai pengganti kelas induk tetapi tidak mengubah perilakunya. pada program ini saya menerapkan pada fitur yang berfungsi memasukan resep, pada class resep akan menurunkan sifatnya. ini tetap memiliki perilaku untuk resep tetapi dapat dimodifikasi fungsinya.
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/LSP.gif)

Interface Segregation Principle (ISP): antarmuka harus dibagi menjadi bagian-bagian yang lebih kecil dan spesifik, sehingga bergantung pada antarmuka yang sesuai dengan kebutuhan. pada program ini saya membuat fitur pindah dengan memasukan class transisi yang spesifik berfungsi untuk berpindah scene.
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/ISP.gif)

Dependency Inversion Principle (DIP): Modul-level tinggi tidak boleh bergantung pada modul-level rendah. padaprogram ini saya menggunakan variabel seperti auth dan DBReference menggunakan tipe abstrak FirebaseAuth dan DatabaseReference yang memungkinkan fleksibilitas dalam penggunaan berbagai implementasi Firebase.
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/Database.gif)

# No.4
Singleton adalah pola desain yang memastikan sebuah kelas memiliki hanya satu instance dan menyediakan akses global ke instance. pada program saya membuat FirebaseManager untuk memastikan bahwa hanya ada satu instance FirebaseManager yang dapat diakses secara global melalui FirebaseManager.GetInstance(). sehingga dapat digunakan secara konsisten tanpa perlu membuat instance baru setiap kali dibutuhkan.
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/Singleton.gif)

# No.5
pada produk ini saya menggunakan koneksi ke database Firebase Realtime Database yang memungkinkan akses dan manipulasi data secara real-time di aplikasi Unity. dengan menggunakan SDK Firebase operasi dasar seperti menyimpan data, memuat data, dan melakukan pencarian telah diimplementasikan. 
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/Database.gif)

# No.6
pada program saya mengimplementasikan operasi CRUD (Create, Read, Update, Delete) yang digunakan untuk mengelola data resep di Firebase Realtime Database yang dapat membuat, membaca, memperbarui, dan menghapus data resep dalam Firebase Realtime Database menggunakan aplikasi Unity. salah satunya adalah metode SimpanButton() digunakan untuk menyimpan data resep baru ke Firebase Realtime Database. Pada setiap pemanggilan metode StartCoroutine, metode UpdateJudul(), UpdateDeskripsi(), UpdateBahan(), dan UpdateCara() dipanggil untuk mengupdate masing-masing properti resep ke dalam database.
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/CRUD.gif)

# No.7
Unity menyediakan komponen UI yang dapat digunakan untuk membuat elemen GUI seperti tombol, teks, gambar, panel, dan lainnya. Selain menggunakan fitur bawaan Unity, juga dapat menggunakan framework dan library pihak ketiga untuk membangun GUI yang lebih kompleks dan interaktif.
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/GUI.gif)

# No.8
pada program saya menggunakan UnityWebRequest dan elemen GUI seperti Text, yang dapat mengambil data dari API dan menampilkannya dalam elemen GUI. sehingga dapat menampilkan data yang diambil melalui koneksi HTTP dalam antarmuka yang dihasilkan.
![](https://gitlab.com/kensabaerendeftnamor/tugasb/-/raw/main/dokumentasi/HTTP.gif)

# No.9
demonstrasi prduk digitla kepada publik

link youtube:
https://youtu.be/ZWouO6fidLs

# No.10
untuk pengembangan produk digital yang saya buat kedepannya akan melibatkan penggunaan machine learning pada produk digital ini.



﻿using System.Collections;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using TMPro;
using System.Linq;

public class FirebaseManager : MonoBehaviour
{
    //Firebase
    [Header("Firebase")]
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;    
    public FirebaseResep Resep;
    public DatabaseReference DBreference;


    //add resep
    [Header("Addresep")]
    public TMP_InputField judulField;
    public TMP_InputField deskripsiField;
    public TMP_InputField bahanField;
    public TMP_InputField caraField;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            InitializeFirebase();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void InitializeFirebase()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            DependencyStatus = task.Result;
            if (DependencyStatus == DependencyStatus.Available)
            {
                auth = FirebaseAuth.DefaultInstance;
                DBReference = FirebaseDatabase.DefaultInstance.RootReference;
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + DependencyStatus);
            }
        });
    }

    //simpan (button)
    public void SimpanButton()
    {
        StartCoroutine(UpdateJudul(judulField.text));
        StartCoroutine(UpdateDeskripsi(deskripsi.text));
        StartCoroutine(UpdateBahan(bahan.text));
        StartCoroutine(UpdateCara(cara.text));
    }

    //tampil semua resep (button)
    public void TampilButton()
    {        
        StartCoroutine(LoadTampilData());
    }

    private IEnumerator UpdateJudulDatabase(string _judul)
    {
        //judul resep di database diupdate
        var DBTask = DBreference.Child("resep").Child(Resep.ResepId).Child("resep").SetValueAsync(_judul);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //judul diupdate
        }
    }

    private IEnumerator UpdateDeskripsi(string _deskripsi)
    {
        //deskripsi resep di database diupdate
        var DBTask = DBreference.Child("resep").Child(Resep.ResepId).Child("deskripsi").SetValueAsync(_deskripsi);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //deskripsi di update
        }
    }

    private IEnumerator UpdateBahan(string _bahan)
    {
        //bahan resep di database diupdate
        var DBTask = DBreference.Child("resep").Child(Resep.ResepId).Child("bahan").SetValueAsync(_bahan);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //bahan diupdate
        }
    }

    private IEnumerator UpdateCara(string _cara)
    {
        //cara resep di database diupdate
        var DBTask = DBreference.Child("resep").Child(Resep.ResepId).Child("cara").SetValueAsync(_cara);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //cara diupdate
        }
    }

  private IEnumerator LoadResepData()
    {
        //Get resep data
        var DBTask = DBreference.Child("resep").Child(Resep.ResepId).GetValueAsync();

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else if (DBTask.Result.Value == null)
        {
            //data belum dimasukan
            judulField.text = "Masukan judul resep . . . .";
            deskripsiField.text = "Masukan Deskripsi dari resep . . . .";
            bahanField.text = "Masukan bahan-bahan yang dibutuhkan. . . . ";
            caraField.text = "Masukan cara membuat masakan. . . . .";
        }
        else
        {
            //data sudah ada
            DataSnapshot snapshot = DBTask.Result;

            judulField.text = snapshot.Child("judul").Value.ToString();
            deskripsiField.text = snapshot.Child("deskripsi").Value.ToString();
            bahanField.text = snapshot.Child("bahan").Value.ToString();
            caraField.text = snapshot.Child("cara").Value.ToString();
        }
    }

    private IEnumerator LoadResepData()
    {
        //Get semua data resep
        var DBTask = DBreference.Child("resep").OrderByChild("judul").GetValueAsync();

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //data diambil
            DataSnapshot snapshot = DBTask.Result;

            //hapus semua data resep
            foreach (Transform child in tampilContent.transform)
            {
                Destroy(child.gameObject);
            }

            //mengulang semua resep yang ada
            foreach (DataSnapshot childSnapshot in snapshot.Children.Reverse<DataSnapshot>())
            {
                string judul = childSnapshot.Child("judul").Value.ToString();
                string deskripsi = childSnapshot.Child("deskripsi").Value.ToString();
                string bahan = childSnapshot.Child("bahan").Value.ToString();
                string cara = childSnapshot.Child("cara").Value.ToString();

                //memperbarui tampilan
                GameObject tampilElement = Instantiate(resepElement, tampilContent);
                tampilElement.GetComponent<ResepElement>().NewResepElement(judul, deksripsi, bahan, cara);
            }

            //menuju screen tampilan
            UIManager.instance.TampilScreen();
        }
    }
}

    private IEnumerator SearchResepData(string keyword)
{
    // Get semua data resep
    var DBTask = DBreference.Child("resep").OrderByChild("judul").GetValueAsync();

    yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

    if (DBTask.Exception != null)
    {
        Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
    }
    else
    {
        // Data diambil
        DataSnapshot snapshot = DBTask.Result;

        // Mengulang semua resep yang ada
        foreach (DataSnapshot childSnapshot in snapshot.Children.Reverse<DataSnapshot>())
        {
            string judul = childSnapshot.Child("judul").Value.ToString();
            string deskripsi = childSnapshot.Child("deskripsi").Value.ToString();
            string bahan = childSnapshot.Child("bahan").Value.ToString();
            string cara = childSnapshot.Child("cara").Value.ToString();

            // Cek apakah judul resep cocok dengan kata kunci pencarian
            if (judul.Contains(keyword))
            {
                // memperbarui tampilan
                GameObject tampilElement = Instantiate(resepElement, tampilContent);
                tampilElement.GetComponent<ResepElement>().NewResepElement(judul, deskripsi, bahan, cara);
            }
        }

        // menuju screen tampilan
        UIManager.instance.TampilScreen();
    }
}





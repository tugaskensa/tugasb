﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    JSONNode jsonData;
    string url;
    public Text title;
    public Text servings;
    public Text times;
    public Text difficulty;
    public Text date;
    public Text author;
    public Text desc;

    void Start()
    {
        url = "https://masak-apa-tomorisakura.vercel.app/api/recipe/resep-nasi-tutug-oncom";
        StartCoroutine(GetDataDariAPI());
    }

   IEnumerator GetDataDariAPI()
   {
    using(UnityWebRequest webData = UnityWebRequest.Get(url))
    {
        yield return webData.SendWebRequest();
        if(webData.isNetworkError || webData.isHttpError)
        {
            print("Tidak ada Koneksi");
        } else {
            if(webData.isDone)
            {
                jsonData = JSON.Parse(System.Text.Encoding.UTF8.GetStrig(webData.downloadHandler.data));
                if(jsonData == null)
                {
                    Debug.Log("Data Json di server Kosong");
                } else {
                title.text = jsonData["results"]["title"].ToString();
                servings.text = jsonData["results"]["servings"].ToString();
                times.text = jsonData["results"]["times"].ToString();
                difficulty.text = jsonData["results"]["dificulty"].ToString();
                author.text = jsonData["results"]["author"]["user"].ToString();
                date.text = jsonData["results"]["date"]["datePublished"].ToString();
                desc.text = jsonData["results"]["desc"].ToString();
                }

            }
        }
    }
   }
}

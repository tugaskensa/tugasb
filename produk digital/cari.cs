﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cari : MonoBehaviour
{
    public void SearchButton()
    {
    string searchKeyword = judulField.text; // Ambil kata kunci pencarian dari input field

    // Hapus semua data resep sebelum melakukan pencarian baru
    foreach (Transform child in tampilContent.transform)
    {
        Destroy(child.gameObject);
    }

    // Panggil metode pencarian dengan kata kunci yang diberikan
    StartCoroutine(SearchResepData(searchKeyword));
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Resep : MonoBehaviour
{

    public TMP_Text judulText;
    public TMP_Text deskripsiText;
    public TMP_Text bahanText;
    public TMP_Text caraText;

    public void NewResepElement (string _judul, string _deskripsi, string _bahan, string _cara)
    {
        judulText.text = _judul;
        deskripsiText.text = _deskripsi;
        bahanText.text = _bahan;
        caraText.text = _cara;
    }

}